"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Cow = exports.Chicken = void 0;
class Chicken {
    cluck() {
        console.log('cluck chicken');
    }
}
exports.Chicken = Chicken;
class Cow {
    moo() {
        console.log('moo cow');
    }
}
exports.Cow = Cow;
//# sourceMappingURL=animals.js.map